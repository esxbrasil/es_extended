Config = {}
Config.Locale = 'br'

Config.Accounts = {
	bank = _U('account_bank'),
	black_money = _U('account_black_money'),
	money = _U('account_money')
}

Config.StartingAccountMoney = {bank = 50000}

Config.EnableSocietyPayouts = false -- pagar da conta da sociedade em que o jogador está empregado? Exigência: esx_society
Config.EnableHud            = true -- ativar o hud padrão? Exibir trabalho e contas atuais (dinheiro sujo, banco e dinheiro)
Config.MaxWeight            = 24   -- o peso máximo do inventário sem mochila
Config.PaycheckInterval     = 7 * 60000 -- com que frequência recebe contracheques em milissegundos
Config.EnableDebug          = false

Config.IncompatibleResourcesToStop = {
	['essentialmode'] = 'ES, para resumir, a estrutura de RP pesada de desempenho que ninguém usa - e a origem dos anúncios ZAP aleatórios indesejados que você está vendo',
	['es_admin2'] = 'Ferramenta de administração para a estrutura antiga do ES que não funcionará com o ESX',
	['esplugin_mysql'] = 'MySQL "plugin" para o antigo framework ES que possui uma vulnerabilidade de injeção SQL',
	['es_ui'] = 'dinheiro HUD para ES'
}
